# Practica 3.1 JDBC

Se va a realizar la creación una base de datos para una liga. Esta base de datos esta compuesta por tres tablas:Liga-Equipo-Jugador. En ellas queda reflejada que la liga esta compuesta por equipos y los equipos estan compuestos por jugadores.

```mermaid
graph TD;
  Liga-->Equipo;
  Equipo-->Jugador;
```

## TABLAS

Nombre de las tablas y sus columnas

|Liga|Equipo|Jugador|
|---|---|---|
|ID|EQUIPOS_ID|JUGADOR_ID|
|NOMBRE_LIGA|NOMBRE_EQUIPO|NOMBRE_JUGADOR|
|PAIS|PUNTUACION|POSICION|
||CIUDAD|NACIONALIDAD|
||NOMBRE_ESTADIO|DORSAL|
||LIGA_ID (FK)|EQUIPOS_ID (FK)|

## Usuario y Contraseña
```
Usuario:root
Contraseña:1234
```


## Cadena de conexion

```
jdbc:mysql://localhost:3307/liga
```

